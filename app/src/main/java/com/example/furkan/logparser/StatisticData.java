package com.example.furkan.logparser;

/**
 * Created by Furkan on 10/13/15.
 */
public class StatisticData {

    private int uniqueIpCount;
    private String theMostWiewingUser;
    private int uniqueContentCount;
    private String theMostWiewedContent;
    private String theMostEnteredBrowser;
    private String theMostViewedBitRate;
    private long totalSentGigabyte;
    private String httpStatus;

    private static StatisticData statisticData;

    private StatisticData()
    {

    }

    public static StatisticData getObject()
    {

        if (statisticData == null)
        {
            statisticData = new StatisticData();
        }
        return statisticData;

    }

    public int getUniqueIpCount()
    {
        return uniqueIpCount;
    }

    public void setUniqueIpCount(int uniqueIpCount)
    {
        this.uniqueIpCount = uniqueIpCount;
    }

    public String getTheMostWiewingUser()
    {
        return theMostWiewingUser;
    }

    public void setTheMostWiewingUser(String theMostWiewingUser)
    {
        this.theMostWiewingUser = theMostWiewingUser;
    }

    public int getUniqueContentCount()
    {
        return uniqueContentCount;
    }

    public void setUniqueContentCount(int uniqueContentCount)
    {
        this.uniqueContentCount = uniqueContentCount;
    }

    public String getTheMostWiewedContent()
    {
        return theMostWiewedContent;
    }

    public void setTheMostWiewedContent(String theMostWiewedContent)
    {
        this.theMostWiewedContent = theMostWiewedContent;
    }

    public String getTheMostEnteredBrowser()
    {
        return theMostEnteredBrowser;
    }

    public void setTheMostEnteredBrowser(String theMostEnteredBrowser)
    {
        this.theMostEnteredBrowser = theMostEnteredBrowser;
    }

    public String getTheMostViewedBitRate()
    {
        return theMostViewedBitRate;
    }

    public void setTheMostViewedBitRate(String theMostViewedBitRate)
    {
        this.theMostViewedBitRate = theMostViewedBitRate;
    }

    public long getTotalSentGigabyte()
    {
        return totalSentGigabyte;
    }

    public void setTotalSentGigabyte(long totalSentGigabyte)
    {
        this.totalSentGigabyte = totalSentGigabyte;
    }

    public String getHttpStatus()
    {
        return httpStatus;
    }

    public void setHttpStatus(String httpStatus)
    {
        this.httpStatus = httpStatus;
    }
}
