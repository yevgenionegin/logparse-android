package com.example.furkan.logparser;


import android.app.ProgressDialog;
import android.os.AsyncTask;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;


/**
 * Created by Furkan on 10/13/15.
 */
public class JsonParseFromWeb {

    StatisticData statisticData = StatisticData.getObject();
    ProgressDialog progressDialog = null;


    public StatisticData getData() throws URISyntaxException, IOException {

        URL jsonUrl = new URL("http://192.168.2.114:8888/rs");

        ObjectMapper mapper = new ObjectMapper();

        statisticData = mapper.readValue(jsonUrl, StatisticData.class);

        return statisticData;

    }


}
